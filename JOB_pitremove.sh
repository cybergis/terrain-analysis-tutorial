#!/bin/bash
### Example job submission on Roger (roger-login.ncsa.illinois.edu)
### {v_name} {v_email}

### Set the job name
#PBS -N {v_job_name}

### Use the bourne shell
#PBS -S /bin/bash

### To send email when the job is completed:
### be --- before execution
### ae --- after execution
#PBS -m ae
#PBS -M {v_email}

### Optionally set the destination for your program's output
### Specify localhost and an NFS filesystem to prevent file copy errors.
### #PBS -e localhost:$HOME/myjob.err
### #PBS -o localhost:$HOME/myjob.log

### Specify the number of cpus for your job.
#PBS -l nodes=2:ppn=20
###printenv

### Tell PBS how much memory you expect to use. Use units of 'b','kb', 'mb' or 'gb'.
###PBS -l mem=8gb

### Tell PBS the anticipated run-time for your job, where walltime=HH:MM:SS
#PBS -l walltime=1:00:00


module load mpich taudem
echo "Getting pit removed DEM"
mpirun -n $PBS_NP pitremove -z {v_uncompress} -fel {v_pitremoved}

sleep 5
echo "Getting slope"
mpirun -n $PBS_NP d8flowdir -fel {v_pitremoved} -sd8 {v_slope} -p {v_aspect}

# End of PBS script