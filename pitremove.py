import os
import sys
import argparse

def main():
        parser = argparse.ArgumentParser()
        parser.add_argument("-n", "--name",
                help="your name (same name as newly created directory with terrain folder content)")
        parser.add_argument("-e", "--email",
                help="your illinois.edu email")
        parser.add_argument("-j", "--job_name",
                help="name of new qsub job")

        args = parser.parse_args()
        name = args.name
        email = args.email
        job_name = args.job_name

        string = open('JOB_pitremove.sh').read().format(v_name=name,
                v_email=email,
                v_job_name=job_name,
                v_uncompress='/home/'+name+'/terrain/usgs/uncompress',
                v_pitremoved='/home/'+name+'/output/pitremoved',
                v_slope='/home/'+name+'/output/slope',
                v_aspect='/home/'+name+'/output/aspect')
        sys.stdout.write(string)

if __name__ == '__main__':
        main()