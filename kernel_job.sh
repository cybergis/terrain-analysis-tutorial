#!/bin/bash
### Example job submission on Roger (roger-login.ncsa.illinois.edu)
### Aiman Soliman (asoliman@illinois.edu)

### Set the job name
#PBS -N job_kernel_yourname

### Use the bourne shell
#PBS -S /bin/bash

### To send email when the job is completed:
### be --- before execution
### ae --- after execution
#PBS -m ae
#PBS -M asoliman@illinois.edu

### Optionally set the destination for your program's output
### Specify localhost and an NFS filesystem to prevent file copy errors.
### #PBS -e localhost:$HOME/myjob.err
### #PBS -o localhost:$HOME/myjob.log

### Specify the number of cpus for your job. 
#PBS -l nodes=2:ppn=20
###printenv

### Tell PBS how much memory you expect to use. Use units of 'b','kb', 'mb' or 'gb'.
###PBS -l mem=8gb

### Tell PBS the anticipated run-time for your job, where walltime=HH:MM:SS
#PBS -l walltime=1:00:00


module load mpich gdal2-stack anaconda java
echo "working"
mpirun -n $PBS_NP /projects/EOT/map-algebra/src/mpi/mapalg-kconvolve -k /projects/EOT/terrain/usgs/uncompress/kernel.csv -i /home/jyn/junjun/output/pitremoved/felr0c0.tif -x 3 -y 3 -b 1 -o /home/jyn/junjun/output/kernel.tif

# End of PBS script
